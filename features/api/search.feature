Feature: Access to the api

  Background:
    Given there is 1 business like:
      | name |
      | Haircut Master |

  @ignore
  Scenario: Search business
    Given there is 10 business
    Given I prepare a GET request on "/api/search/businesses"
    When I send the request
    Then I should receive a 200 json response
    Then the JSON response should contain:
      | total | 11 |

  @ignore
  Scenario: Search services
    Given there is 5 services like:
      | business |
      | Haircut Master |
    Given I specified the following request queries:
      | location[lat] | 40 |
      | location[lon] | -70 |
      | searchTerm | taxon |
      | searchParam | cleaning |
    Given I prepare a GET request on "/api/search/services"
    When I send the request
    Then I should receive a 200 json response
    Then the JSON response should contain:
      | total | 5 |

  @ignore @reset-schema
  Scenario: Search services by business
    Given there is 1 business like:
      | name |
      | Beauty |
    Given there is 5 services like:
      | business |
      | Haircut Master |
    Given there is 2 services like:
      | business |
      | Beauty |
    Given I specified the following request queries:
      | location[lat] | 40 |
      | location[lon] | -70 |
      | searchTerm | taxon |
      | searchParam | cleaning |
      | criteria[business] | 2 |
    Given I prepare a GET request on "/api/search/services"
    When I send the request
    Then I should receive a 200 json response
    Then the JSON response should contain:
      | total | 2 |
